/**
 * Created by alecg on 10/17/2016.
 */
var express = require("express");
var router = express.Router();
var dSite = require("../../db/site");

router.get("/", function(req,res,next){
    dSite.getSites(req.user.local.email, function(e,d){
        if (e) return next(e);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({
            sites: d
        }));
    });
});



module.exports = router;