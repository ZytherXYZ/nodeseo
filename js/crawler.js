/**
 * Created by alecg on 10/10/2016.
 */

var Crawler = require("js-crawler");



function crawlPage(tURL, tFilter, cb) {
    var tCrawl = new Crawler();
    tCrawl.configure({
        maxRequestsPerSecond: 5,
        maxConcurrentRequests: 5,
        depth: 3,
        shouldCrawl: function(url){
            if (tFilter === null){
                return true;
            } else {
                return url.indexOf(tFilter) > 0;
            }
        }
    });
    var tArray = [];
    var tSuccess = [];
    var tFail = [];
    tCrawl.crawl({
       url: tURL,
        success: function(pg){
            console.log("Crawled page!" + pg.url);
            tSuccess.push(pg.url);
        },
        failure: function(pg){
            console.log("did not crawl" + pg.status + " " + pg.url);
            var ttFail = {status: pg.status, url: pg.url};
            tFail.push(ttFail);
        },
        finished: function(list) {
            console.log("done!");
            console.dir(list);
            tArray = {
                urls: tSuccess,
                fails: tFail,
                all: list
            };
            return cb(null, tArray, true);

        }
    });
}

exports.crawlPage = crawlPage;