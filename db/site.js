/**
 * Created by alecg on 10/17/2016.
 */

var aSite = require('../models/site');



function getSites(uEmail, cb){
    aSite.find({users: uEmail}, function(err, sites){
        if (err) return cb(err, null);
        if (sites) {
            return cb(null, sites);
        } else {
            return cb(null, null);
        }
    });
}

exports.getSites = getSites;
