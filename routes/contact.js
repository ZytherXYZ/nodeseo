/**
 * Created by alecg on 10/18/2016.
 */
var express = require("express");

var router = express.Router();

router.get("/", function(req,res,next){
    res.render("contact", {
        user: req.user,
        messages: {
            info: req.flash("info"),
            error: req.flash('error')
        }
    });
})



module.exports = router;