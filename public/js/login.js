/**
 * Created by alecg on 10/11/2016.
 */
//login.js

$(document).ready(function() {
    $('.ui.form.login').form({
        fields: {
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: "Please enter a legit email."
                    }
                ]
            },
            password: {
                idendifier: 'password',
                rules: [
                    {type: 'empty', prompt: "Please enter a legit password."}
                ]
            }
        }
    });
});