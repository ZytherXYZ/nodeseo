/**
 * Created by alecg on 10/19/2016.
 */
var jobCrawlFull = require("../jobCrawlFull");


module.exports = function(agenda){
    agenda.define("crawlSite", function(job, done){
        //console.dir("crawler started");
        console.dir(job.attrs.data);
        jobCrawlFull.jobCrawlFull(job.attrs.data, function(e,d,isDone){
            if (e) {
                job.fail(e);
                job.save();
            }
            if (isDone) {
                done();
                //done()
            }
        });
    });
};