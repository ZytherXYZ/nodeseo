/**
 * Created by alecg on 10/16/2016.
 */

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var nodemailer = require('nodemailer');
var sgTransport = require("nodemailer-sendgrid-transport");
var async = require('async');

router.get("/:token", function(req,res,next){
    if (!(req.isAuthenticated())){
        User.findOne({resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()}}, function(err,user){
            if (!user){
                req.flash("error", "Password reset token is invalid or has expired.");
                return res.redirect("/forgot");
            }
            res.render("reset", {
                user: req.user,
                messages: {
                    error: req.flash("error"),
                    info: req.flash("info")
                }
            });
        });
    } else {
        req.flash("info", "You're already logged in, silly!");
        res.redirect("/dashboard");
    }
});


router.post("/:token", function(req, res,next){
    async.waterfall([
        function(done){
        if (req.body.password.length < 6){
            req.flash("error", "Password must be 6 chars or more.");
            return res.redirect("/reset/" + req.params.token);
        }
        if (req.body.password !== req.body.passwordagain){
            req.flash("error", "Stop this nonsense, and input your password twice. Thanks.");
            return res.redirect("/reset/" + req.params.token);
        }
            User.findOne({resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now()}}, function(err, user){
                if (!user){
                    req.flash("error", 'Password reset token is invalid or has expired.');
                    return res.redirect("/forgot");
                }
                user.local.password = user.generateHash(req.body.password);
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;
                user.save(function(err){
                    done(err, user);
                });
            });
        },
        function(user,done){
            var sOptions = {
                auth: {
                    api_key: "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"
                }
            };

            var client = nodemailer.createTransport(sgTransport(sOptions));

            var mailOptions = {
                to: user.local.email,
                from: "nodeseo@zyther.xyz",
                subject: "NodeSEO Password Reset",
                text: 'Hello,\n\n' +
                'This is a confirmation that the password for your NodeSEO account ' + user.local.email + ' has just been changed.\n'
            };
            client.sendMail(mailOptions, function(err){
                req.flash('info', 'Your password has been changed.');
                done(err, 'done');
            });
        }
    ], function(err){
        if (err){
            return next(err);
        }
        res.redirect("/login");
    });

});

module.exports = router;

