/**
 * Created by alecg on 10/27/2016.
 */
var express = require("express");
var router = express.Router({mergeParams:true});

router.get("/", function(req,res,next){
    res.render("dash/site/auditor", {
        site: req.agData,
        user: req.user
    });
});

module.exports = router;