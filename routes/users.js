var express = require('express');
var router = express.Router();
var pCrawl = require("./dash/crawl");
var pSite = require("./dash/site");




router.get('/', function(req, res, next) {
  res.render("dashboard.ejs", {user: req.user, messages: {error: req.flash("error"), info: req.flash("info")}});
});

router.use("/crawl", pCrawl);
router.use("/site", pSite);



module.exports = router;
