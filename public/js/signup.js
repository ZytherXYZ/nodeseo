/**
 * Created by alecg on 10/12/2016.
 */
//signup.js

$(document).ready(function() {
    $('.ui.form.signup').form({
        fields: {
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: "Please enter a legit email."
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {type: 'minLength[6]', prompt: "Please enter a password 6 characters or more."},
                    {type: 'empty', prompt: "Please enter a legit password."}
                ]
            }
        }
    });
});