/**
 * Created by alecg on 10/11/2016.
 */
var express = require('express');
var router = express.Router();
var passport = require("passport");

router.get("/", function(req,res,next){
    if (req.isAuthenticated()){
        res.redirect("/dashboard");
    } else {
        res.render("login.ejs", {messages: {
            error: req.flash("error"),
            info: req.flash("info")
        }});
    }
});

router.post('/', passport.authenticate('local-login', {
    successRedirect : '/dashboard',
    failureRedirect : '/login',
    failureFlash : true // allow flash messages
}));

module.exports = router;