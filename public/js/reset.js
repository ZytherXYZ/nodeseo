/**
 * Created by alecg on 10/16/2016.
 */
$(document).ready(function() {
    $('.ui.form.resetForm')
        .form({
            fields: {
                password: {
                    identifier: 'password',
                    rules: [
                        {type: 'empty', prompt: "Please enter a legit password."},
                        {type: "minLength[6]"}
                    ]
                },
                passwordagain: {
                    identifier: 'passwordagain',
                    rules: [
                        {type: 'match[password]', prompt: "Please enter the same password twice"}

                    ]
                }
            }
        });
});