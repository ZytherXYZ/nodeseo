var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');



var pContact = require("./routes/contact");
var pD = require("./routes/d");
var pSignup = require("./routes/signup");
var pLogin  = require("./routes/login");
var pLogout = require("./routes/logout");
var pForgot = require("./routes/forgot");
var pReset = require("./routes/reset");


var aG = require("./agenda/agenda");

var app = express();

// db and auth stuff
var mongoose = require("mongoose"),
    passport = require("passport"),
    flash = require("connect-flash"),
    session = require("express-session");

require('./config/passport')(passport);


var configDB = require("./config/db.js");

mongoose.connect(configDB.url);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//more db and sesh stuff
app.use(session({secret: "thisisasecretwhatthefxck"}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());




//pages
app.use('/', routes);

app.use("/contact", pContact);

app.use('/dashboard', isLoggedIn, users);

// data for the user
app.use('/d', isLoggedIn, pD);

app.use("/signup", pSignup);

app.use("/login", pLogin);
app.use("/logout", pLogout);
app.use("/forgot", pForgot);
app.use("/reset", pReset);

function isLoggedIn(req,res,next){
  if (req.isAuthenticated()){
    return next();
  } else {
    res.redirect("/");
  }
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
