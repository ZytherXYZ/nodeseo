var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.isAuthenticated()) {
    res.render('index', {
      title: 'NodeSEO',
      user: req.user,
      messages: {
        error: req.flash("error"),
        info: req.flash("info")
      }});
  } else {
    res.render('index', {
      title: 'NodeSEO',
      messages: {
        error: req.flash("error"),
        info: req.flash("info")
      }});
  }
});

module.exports = router;
