/**
 * Created by alecg on 10/12/2016.
 */
//models/user.js

var mongoose    = require("mongoose");
var bcrypt      = require("bcrypt-nodejs");

var userSchema = mongoose.Schema({
    local : {
        email : String,
        password: String
    },
    google : {
        id : String,
        token: String,
        email: String,
        name: String
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date
});

userSchema.methods.generateHash = function(pw){
    return bcrypt.hashSync(pw, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(pw){
    return bcrypt.compareSync(pw, this.local.password);
};

module.exports = mongoose.model("User", userSchema);