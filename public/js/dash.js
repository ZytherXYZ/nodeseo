/**
 * Created by alecg on 10/17/2016.
 */

$(document).ready(function(){
    $("#theButton").click(function(){
        toCrawl();
    });

    $("#theURL").on("keypress", function(e){
        if (e.which === 13){
            toCrawl();
        }
    });


    $("#theSites").html("Loading...");
    getSites();

});

function toCrawl(){
    var tt = $("#theURL").val();
    if (tt.split(".").length === 2){
        var tt = "www." + tt;
    }
    window.location = "/dashboard/crawl/" + tt;
}

function getSites(){
    $.get("/d/siteList", function(data){
        console.dir(data);
            var sitesTemplate = $("#sitesTemplate").html();
            Mustache.parse(sitesTemplate);
            var rendered = Mustache.render(sitesTemplate, data);
            $("#theSites").html(rendered);

    });
}
