/**
 * Created by alecg on 10/15/2016.
 * forgot.js
 * Add functionality to reset user password
 */

var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var User = require('../models/user');
var nodemailer = require('nodemailer');
var sgTransport = require("nodemailer-sendgrid-transport");
var async = require("async");


router.get("/", function(req,res,next){
    res.render('forgot', {
        user: req.user,
        messages: {
            info: req.flash("info"),
            error: req.flash("error")
        }
    });
});


router.post("/", function(req,res,next){
   async.waterfall([
        function (done){
            crypto.randomBytes(20, function(err, buf){
                var token = buf.toString('hex');
                done(err, token);
            })
        },
       function(token, done){
            User.findOne({'local.email' : req.body.email}, function(err,user){
                if (!user){
                    req.flash('error', 'No account with that email exists!');
                    return res.redirect("/forgot");
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000;

                user.save(function(err){
                    done(err, token, user);
                })
           })
       },
       function(token, user, done){
           var sOptions = {
               auth: {
                   api_key: "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"
               }
           };

           var client = nodemailer.createTransport(sgTransport(sOptions));

           mailOptions = {
               to: user.local.email,
               from: "nodeseo@zyther.xyz",
               subject: "NodeSEO Password Reset",
               text: 'You are receiving this because you (or someone else) have requested the reset of the password for your NodeSEO account.\n\n' +
               'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
               'http://' + req.headers.host + '/reset/' + token + '\n\n' +
               'If you did not request this, please ignore this email and your password will remain unchanged.\n'
           };
           client.sendMail(mailOptions, function(err){
               req.flash('info', 'An e-mail has been sent to ' + user.local.email + ' with further instructions.');
               done(err, 'done');
           });
       }
   ], function(err){
       if (err){
           console.error(err);
           return next(err);
       }
        res.redirect("/forgot");
   });
});


module.exports = router;