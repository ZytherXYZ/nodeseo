/**
 * Created by alecg on 10/17/2016.
 */
var express = require("express"),
    router = express.Router();

var siteList = require('./d/siteList');

router.get("/", function(req,res,next){
    //todo something
    req.flash("error", "How did you even do that?");
    res.redirect("/dashboard");
});

router.use('/siteList', siteList);

module.exports = router;