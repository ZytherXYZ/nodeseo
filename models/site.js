/**
 * Created by alecg on 10/16/2016.
 */

var mongoose = require("mongoose");

var siteSchema = mongoose.Schema({
    siteName: String,
    siteURL: String,
    filter: String,
    userGroups: [],
    users: [],
    currentState: String,
    waitingToCrawl: Boolean,
    crawlResults: [],
    updated: { type: Date, default: Date.now()}
});


siteSchema.statics.setStatus = function(url, users, status) {
    if (!url || !users || !status) {
        console.log("Did not set status. URL: " + url + " USERS: " + users + " STATUS: " + status);
        return;
    }
    this.findOne({siteURL: url, users: users}, function(err, doc){
        if (err) {
            console.error(err);
            console.log("Did not set status.");
            return;
        }
        if (!doc){
            console.log("Did not set status. no doc.");
            return;
        }
        switch (status) {
            case "crawling":
                doc.currentState = status;
                doc.save();
                return;
            case "crawled":
                doc.currentState = status;
                doc.save();
                return;
            case "auditing":
                doc.currentState = status;
                doc.save();
                return;
            case null:
                return;

            default:
                return;
        }
    });

};


siteSchema.statics.saveQueue = function(url, users, crawlData, cb){
    if (!crawlData) return cb(new Error("No crawl Data"), null);

    this.findOne({siteURL: url, users: users}, function (err, doc) {
        if (err) return cb(err, null, true);
        if (!doc) {
            console.log("no doc, wtf");
            return cb(new Error("No Doc"), null, true);
        }

        doc.crawlResults = crawlData;
        doc.currentState = "crawled";
        doc.waitingToCrawl = false;

        doc.save(function (error, dox) {
            if (error) return cb(error, null.true);
            return cb(null, dox, true);
        });
    });
};



module.exports = mongoose.model("Site", siteSchema);