/**
 * Created by alecg on 10/17/2016.
 */
var express = require('express');
var router = express.Router();

var validURL = require("valid-url");

var aSite = require("../../models/site");

var ag = require("../../agenda/agenda");

router.get("/", function(req,res,next){
    req.flash("error", "URL cannot be nothing.");
    res.redirect("/dashboard");
});

router.get("/:site", function(req,res,next){
    if (validURL.isUri("http://" + req.params.site)) {
        aSite.findOne({siteURL: req.params.site, users: req.user.local.email}, function (err, site) {
            if (err) return next(err, null);
            if (site) {
                req.flash("error", "Site already exists!");
                res.redirect("/dashboard");
            } else {
                res.render("dash/crawl", {
                    messages: {
                        info: req.flash("info"),
                        error: req.flash("error")
                    },
                    site: req.params.site,
                    user: req.user
                });
            }
        });
    } else {
        req.flash("error", "Not a valid URL. Try again!");
        res.redirect("/dashboard");
    }
    //res.send(req.params.site);
});

router.post("/:site", function(req,res,next){
    console.dir(req.params);
    console.dir(req.body);

    if (validURL.isWebUri("http://" + req.params.site)) {
        aSite.findOne({siteURL: req.params.site, users: req.user.local.email}, function (err, site) {
            if (err) return next(err, null);
            if (site) {
                req.flash("error", "Site already exists!");
                res.redirect("/dashboard");
            } else {
                var tSite = new aSite();
                tSite.siteName = req.body.siteName;
                tSite.siteURL = req.params.site;
                var tt = req.params.site;
                var tq = req.params.site.split(".");
                if ((tq[0]) === 'www') {tq = tq.splice(1,2).join(".");
                } else {tq=tt}
                tSite.filter = tq;
                tSite.users.push(req.user.local.email);
                tSite.currentState = "queued";
                tSite.waitingToCrawl = true;

                tSite.save(function(err){
                    if (err) return next(err);
                    else {
                        ag.now("crawlSite", {
                            siteURL: req.params.site,
                            siteName: req.body.siteName,
                            users: req.user.local.email,
                            filter: tq
                        });
                        req.flash("info", "Queued for crawling!");
                        res.redirect("/dashboard");
                    }
                });

            }
        });
    } else {
        req.flash("error", "Not a valid URL. Try again!");
        res.redirect("/dashboard");
    }

});



module.exports = router;