/**
 * Created by alecg on 10/21/2016.
 */
var express = require('express');
var router = express.Router({mergeParams: true});


var pConfigure = require("./site/configure");
var pAuditor = require("./site/auditor");

var Site = require("../../models/site");


function getSite(req,res,next){
    Site.findOne({siteURL: req.params.site, users: req.user.local.email}, function(err, s){
        if (err) return next(err);
        if (!s) {
            req.flash("error", "Not allowed");
            res.redirect("/dashboard");
        }
        req.agData = s;
        return next();
    });
}

router.get("/", function(req,res,next){
    req.flash("error", "Try selecting a site next time.");
    res.redirect("/dashboard");
});

router.use("/:site/configure", getSite, pConfigure);
router.use("/:site/auditor", getSite, pAuditor);

router.get("/:site", getSite, function(req,res,next){
        res.render("dash/site", {
            site: req.agData,
            user: req.user
        });

});



module.exports = router;