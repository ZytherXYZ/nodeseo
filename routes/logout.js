var express = require('express');
var router = express.Router();

router.get("/", function(req,res,next){
    req.logout();
    req.flash("info", "Successfully logged out. Come back soon!");
    res.redirect("/");
});

module.exports = router;