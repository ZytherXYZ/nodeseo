/**
 * Created by alecg on 10/18/2016.
 */
var Agenda = require("agenda");
var db = require("../config/db");
var dbURL = db.url;


var agenda = new Agenda({
    db: {
        address: dbURL
    }
});

require("./jobs/crawlSite")(agenda);


agenda.on('ready', function(){
    console.log("Message queue (agenda) started.");
    agenda.processEvery("10 seconds");
    agenda.start();
});


agenda.on("success:crawlSite", function(job){
    console.log("crawlSite done for %s", job.attrs.data.siteURL);
    job.remove(function(e){
        if (!e) {
            console.log("Removed crawl job.");
        } else throw new e;

    })
})






module.exports = agenda;
