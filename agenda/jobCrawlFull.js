/**
 * Created by alecg on 10/18/2016.
 */

var crawler = require("../js/crawler");
var Site = require("../models/site");


function jobCrawlFull(d, cb) {
    //console.dir(d);
    Site.setStatus(d.siteURL, d.users, "crawling");
    crawler.crawlPage("http://" + d.siteURL, d.filter, function(e,dd,isDone){
        if (e) return cb(e, null, true);
        if (isDone){
            Site.saveQueue(d.siteURL, d.users, dd, function(e,d,isDone){
                if (e){
                    return cb(e, null, true);
                }
                if (isDone){
                    return cb(null,d,true);
                }
            })
        }
    });
    //crawler.crawlPage()
}

exports.jobCrawlFull = jobCrawlFull;