/**
 * Created by alecg on 10/11/2016.
 */

var express = require('express');
var router = express.Router();
var passport = require("passport");

router.get("/", function(req,res,next){
    res.render("signup.ejs", {messages: {
        info: req.flash("info"),
        error: req.flash("error")
    }});
});


router.post("/", passport.authenticate('local-signup', {
    successRedirect: '/dashboard',
    failureRedirect: '/signup',
    failureFlash: true
}));


module.exports = router;